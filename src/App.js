import React, { Component } from 'react';

class App extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <h1>React App</h1>
          <div className="alert alert-success alert-dismissable fade show" role="alert">
            <button type="button" className="close" data-dismiss="alert">
              <span aria-hidden="true">&times;</span>
            </button>
            <p>With Bootstrap!</p>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
